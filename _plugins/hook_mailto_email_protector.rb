Jekyll::Hooks.register([:pages, :posts, :documents], :pre_render) do |post|
  # This will run every time the website is built

  # Include a plugin (needs to be in gemfile)
  include Jekyll::EmailProtect::EmailProtectionFilter

  # Using a simpler version of email regexp
  email_regexp = /mailto\:(?:[\'\"]*)(?:[\w+\-]\.?)+@[a-z\d\-]+(?:\.[a-z]+)*\.[a-z]+(?:[\'\"]*)/i

  # Take post's content, and transform every occurence of the following regexp (an email)
  post.content = post.content.gsub(email_regexp) {
    # For every occurence, apply protection function:
    |param| "mailto:" + Jekyll::EmailProtect::EmailProtectionFilter::encode_email(param[7..-1].delete("'").delete('"'))
  }
end

