
<a name="CM"></a>
# 5 Contract Management

The Contract Management module allows recording legal documents signed in the context of research activities. Contracts are typically linked to Projects and provide the necessary traceability for the GDPR compliant provision and transfer of data.

<a name="CM1"></a>
## 5.1 Create New Contract

<mark>In order  to create a new contract</mark>:

1. Click Contracts from the Menu Bar.<br />
 ![Alt](../../external/img/contract_menubar.png)
2. Click the add button from the Contract Search Page.<br />
 ![Alt](../../external/img/add_button.png)
3. You will see an empty Contract Form. The _Project_ field is optional, meanwhile, in practice most contracts are signed in the context of a research project. In the _Roles_ field,  you are expected to select one or more GDPR role that identifies your institutions roles as described in the Contract.  In the _Other comments_ section you may describe the nature of the document or if the document has an ID/REF e.g. from a document management system, you may put it in. Just like projects and datasets, when creating contracts you are expected to provide a local responsible in the _Local Custodians_ field. As stated before, one of the Local Custodians must be a user with VIP Privileges. E.g. in the demo deployment _alice.white_ is a  research principle investigator and is a VIP user.<br />
![Alt](../../external/img/contract_add_form.png)
4. Click SUBMIT. Once you successfully save the form, you will be taken to the newly created contract's details page, as seen below.<br />
![Alt](../../external/img/contract_created.png)

<a name="CM2"></a>
## 5.2 Manage Contract Details

After initial creation the contract will be in a skeletal form and would need further input on its signatories and document attachments.

### 5.2.1 Manage Contract Partners (Signatories)

Contracts have multiple signatories. These can be  managed via the **Partners (Signatories)** detail box.


1. Click the plus button  on the **Partners (Signatories)** details box, as seen below.<br />
 ![Alt](../../external/img/contract_add_partner.png)
2. You will see the **Partner and role** addition form. In this form, you will be asked to select the _Partner_ as well as the  GDPR _Roles_ that this partner assumes in the contract. You can select more than one role. It is also mandatory to provide a contact person that is with the selected partner institute. You can either select from the list or you can add a new contact if it does not already exist.
![Alt](../../external/img/contract_add_partner_form.png)
3. Once you fill in the information and click SUBMIT the partner will be added to the list of signatories, as seen below. Partners can be removed from a contract by clicking on the trash icon that will appear when hovering over the items in the **Partner and role detail box**.<br />
![Alt](../../external/img/project_ref_user_search.png)

### 5.2.2 Manage Contract Documents

You may attach  PDF, word documents, scans, via the **Documents** detail box. Document management is common throughout DAISY modules. It is described [here](#PM25).




<br />
<br />
<br />
<div style="text-align: right;"> <a href="#top">Back to top</a> </div>
<br />
<br />
<br />