---
layout: page
permalink: /external/daisy/

---
# <a name="top"></a>DAISY User Guide
{:.no_toc}
This howto-card is the user guide for the Data Information System (DAISY). If it is your first time with the guide then start with Section **DAISY at a Glance**.

* TOC
{:toc}

{% include_relative at_a_glance.md %}
{% include_relative interface_conventions.md %}
{% include_relative project_management.md %}
{% include_relative dataset_management.md %}
{% include_relative contract_management.md %}
{% include_relative definitions_management.md %}
{% include_relative user_management.md %}


<!--- Below fragment is needed for TOC in iAWriter PDF export

{{TOC}}

/at_a_glance.md "DAISY at a glance"
/interface_conventions.md "Interface Conventions"
/project_management.md "Project Management"
/dataset_management.md "Dataset Management"
/contract_management.md "Contract Management"
/definitions_management.md "Definitions Management"
/user_management.md "User and Permission Management"

+++

-->

