# AsperaWEB Quick Guide

{:toc}

## Overview
AsperaWEB is an IBM Aspera deployment at the LCSB-University of Luxembourg. AsperaWEB supports end-to-end encrypted data transfer and can handle high data volumes e.g. several tera bytes.

## Obtaining a AsperaWEB access link

You need an **access link** to use AsperaWEB. An access link is a temporary, password-protected space, much like a drop box, on LCSB's AsperaWEB server. In order to obtain an access link, you should contact your research collaborator at the LCSB-University of Luxembourg. Once created, you will receive your **access link** and associated **password** (link) by e-mail.

> **IMPORTANT NOTE: Whenever the LCSB shares a password for a AsperaWEB endpoint (collaborator), the password is transmitted via a link which will expire. Therefore you should make a record of your password once you view it.**

An access link can be reached via standard web browsers. Data can be transferred to/from an access link in two ways:

* Through the web browser by visiting the link, which is our recommended way of data transfer, described in this [section of the guide](#ASPERAWEB_WEB)
* Through the use of a command line tool. If your data sits in an environment, where you can not launch a web browser, then you may use use a command line client tool to reach an access link. This process is described in this [section of this guide](#ASPERAWEB_CLI).

The use of DUMA is mediated by LCSB's data stewards. If you require assistance in using DUMA, you should send an email to [LCSB admin team](mailto:lcsb-sysadmins@uni.lu).

<a name="ASPERAWEB_WEB"></a>
## Accessing AsperaWEB via Web Interface

In the following steps we provide instructions on how to use AsperaWEB web interface.

1. Once you receive your **access link** and **password** from the LCSB, visit the link using a standard web browser. Firefox 66.x (or higher) recommended, but others should also work. You will be prompted for your password (see below).<br/>
 ![Alt](../../external/img/duma_1.png "Title")

2. When you visit a AsperaWEB access link for the very first time, you will be prompted to install or update **IBM Aspera Connect** client.
	* click **Download** or **Download the App** buttons (see below). <br/>![Alt](../../external/img/duma_2.png "Title")

	* wait for the download to finish, the prompt should go to step 3. <br/>![Alt](../../external/img/duma_3.png "Title")

	* open the installer just downloaded and start the installer. <br/>![Alt](../../external/img/duma_4.png "Title")

	* close the installer, the Aspera Connect should try to open - *depending on your Operating System you may be asked if you are sure to open it* -

	* the prompt should disapears


3. The **access link** page will display a **File Browser** section. Depending on the settings per access link, users can create or delete folders in the File Browser and upload and/or download data.<br/>
  ![Alt](../../external/img/duma_6.png "Title")


4. Clicking **Upload** or **Download** will launch the **IBM Aspera Connect** client on your computer. You first will be asked whether you allow the client to connect to aspera.lcsb.uni.lu. Choose **Allow**.



5. At any time you can launch **IBM Aspera Connect** to display the status of uploads to or downloads from your computer. <br/>
  ![Alt](../../external/img/duma_8.png "Title")



6. All data are encrypted on server side and they stay encrypted also upon download. For decryption, you have to navigate into your **IBM Aspera Connect** window and click "**Unlock encrypted files**". <br/>
  ![Alt](../../external/img/duma_9.png "Title") <br/>
You will be prompted for encryption passphrase which is present on the file browser. <br/>
  ![Alt](../../external/img/duma_10.png "Title") <br/>
Encrypted files are by default kept on your disc after decryption. If you want to change this behaviour, navigate to Options->Settings and check "Delete encrypted files when finished" box.


7. You can also navigate to the help section on the top right of the browser. It contains information and links to get support.

<a name="ASPERAWEB_CLI"></a>
## Accessing AsperaWEB via Command-Line Tool


Go to the help section of your access link

<br/> ![Alt](../../external/img/duma_help_link.png "Title") <br/><br/><br/>

And follow the steps in the **Using the command line** section.

<br/> ![Alt](../../external/img/duma_cli_tokens.png "Title") <br/><br/><br/>
